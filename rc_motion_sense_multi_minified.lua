common = {}
common.event = require("event")
common.cmp = require("component")
common.sides = require("sides")
common.default_side = common.sides.undefined
common.r_on = 15
common.r_off = 0
common.r_timeout = 1

common.red_actions = {}


common.sensors = {
  [common.cmp.motion_sensor.address] = {
    {
      red = common.cmp.redstone,
      side = common.sides.top,
      action = "button"
    }
  }
}
function common.red_actions.set_side(a,b)a.red.setOutput(a.side,b)a.states.current=b end;function common.red_actions.set_all_sides(a,b)for c=0,5 do a.red.setOutput(c,b)end;a.states.current=b end;function common.red_actions.process_controller(a,d)if not a.states then a.states={}end;local e=a.states.active or common.r_on;local f=a.states.inactive or common.r_off;local g=a.states.init or common.r_off;local h=common.sides[a.side]and a.side~=common.sides.undefined;local i;if h then i=common.red_actions.set_side else i=common.red_actions.set_all_sides end;if d=="init"then i(a,g)a.states.current=g elseif d=="button"then local j=a.timeout or common.r_timeout;local function k()i(a,f)end;i(a,e)common.event.timer(common.r_timeout,k,j)elseif d=="lever"then if a.states.current==e then i(a,f)else i(a,e)end end end;function common.red_actions.process_sensor(l)for c,m in ipairs(common.sensors[l])do common.red_actions.process_controller(m,m.action)end end;function common.red_actions.init_sensors()for n,o in pairs(common.sensors)do for p,m in ipairs(o)do common.red_actions.process_controller(m,"init")end end end;function start(q)local r={["man_cubus"]=true}local s=0;for t in pairs(common.sensors)do local n=common.cmp.proxy(t)n.setSensitivity(s)end;local function u(v)return true end;function common.motion_handler(...)local w={...}if u(w[6])then common.red_actions.process_sensor(w[2])end end;common.red_actions.init_sensors()common.event.listen("motion",common.motion_handler)end;function stop()common.event.ignore("motion",common.motion_handler)common.red_actions.init_sensors()end
common = {}
common.event = require("event")
common.cmp = require("component")
common.side = require("sides").top
common.red = common.cmp.redstone
common.r_on = 15
common.r_off = 0
 
function common.reset_red()
  for i = 0, 5 do --хотя вообще-то так делать не стоит
    common.red.setOutput(i, common.r_off)
  end
end
 
function common.red_on()
  common.red.setOutput(common.side, common.r_on)
end
 
function common.red_off()
  common.red.setOutput(common.side, common.r_off)
end
 
function start(args)
  local valid_users = {["man_cubus"] = true}
  local sensitivity = 64
  local r_timeout = 1
  local sensor = common.cmp.motion_sensor
  sensor.setSensitivity(sensitivity)
 
  local function valid(name)
    return valid_users[name]
  end
 
  function common.motion_handler(...)
    local evt = {...}
    local function off()
      common.red_off()
    end
    if valid(evt[6]) then
      common.red_on()
      common.event.timer(r_timeout, off, 1)
    end
  end
 
  common.reset_red()
  common.event.listen("motion", common.motion_handler)
end
 
function stop()
  common.event.ignore("motion", common.motion_handler)
  common.red_off()
end
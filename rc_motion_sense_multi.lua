--[[
  Все полные адреса считывать анализатором
  В таблице sensors для каждого сенсора должны быть записи вида
  ["полный-адрес-сенсора"] = {
    {--таблица реагирующих на сенсор редстоун-контроллеров
      {--запись о редстоун-контроллере
        red = common.cmp.proxy("полный-адрес-блока-управления-красным-сигналом"), -- это обязательное значение
        side = common.sides.top, -- необязательное значение. Если управляющую сторону блока не задавать, сигнал будет излучаться с каждой стороны
        action = "button", -- необязательное значение "button" | "lever"
        states = {active = 12, inactive = 0, init = 0} -- для тех, кому уровень сигнала 15 - это слишком много
        -- инвертирование делается с помощью, например active = 0, inactive = 15
      }
    }
  }
--Примеры
sensors = {
  [20a74cd8-dc4a-44bf-b9d6-4bba215cd75d] = {
    --простой вариант "кнопка"
    {
      red = common.cmp.proxy("694b30a0-cc84-4453-8a69-5dcc085f3747"),
      side = common.sides.top,
      action = "button"
    },
    --"инвертированная кнопка" с задержкой в 2 секунды
    {
      red = common.cmp.proxy("525cc12d-7f75-48c0-9e69-3fa7639cecc2"),
      side = common.sides.top,
      action = "button",
      timeout = 2,
      states = {active = common.r_off, inactive = common.r_on, init = common.r_on}
    },
    --"рычаг", который изначально включен и потом переключается по сенсору
    {
      red = common.cmp.proxy("80867067-ae2e-4bcd-9c04-141a8a5d6630"),
      side = common.sides.top,
      action = "lever",
      states = {init = common.r_on}
    }
  }
}


--По умолчанию настроен на первый случайный подключенный сенсор и первый случайный блок управления красным камнем
]]



common = {}
common.event = require("event")
common.cmp = require("component")
common.sides = require("sides")
common.default_side = common.sides.undefined
common.r_on = 15
common.r_off = 0
common.r_timeout = 1

common.red_actions = {}


common.sensors = require("sensor_config")

-- init, button, lever

function common.red_actions.set_side(r_controller, state)
  r_controller.red.setOutput(r_controller.side, state)
  r_controller.states.current = state
end

function common.red_actions.set_all_sides(r_controller, state)
  for i = 0, 5 do
    r_controller.red.setOutput(i, state) --так по-прежнему не стоит делать
  end
  r_controller.states.current = state
end

function common.red_actions.process_controller(r_controller, action)
   --[[ при желании можно определить собственный уровень активного или неактивного сигнала
   и сохранить его в этих полях записи о контроллере]]
  if not r_controller.states then r_controller.states = {} end
  local active = r_controller.states.active or common.r_on
  local inactive = r_controller.states.inactive or common.r_off
  local init = r_controller.states.init or common.r_off
  local side_defined = (common.sides[r_controller.side] and r_controller.side ~= common.sides.undefined)

  local setter

  if side_defined then
    setter = common.red_actions.set_side
  else
    setter = common.red_actions.set_all_sides
  end
  if action == "init" then
    setter(r_controller, init)
    r_controller.states.current = init
  elseif action == "button" then
    local timeout = r_controller.timeout or common.r_timeout
    local function off()
      setter(r_controller, inactive)
    end
    setter(r_controller, active)
    common.event.timer(common.r_timeout, off, 1)
  elseif action == "lever" then
    if r_controller.states.current == active then
      setter(r_controller, inactive)
    else
      setter(r_controller, active)
    end
  end
end

function common.red_actions.process_sensor(sensor_address)
  for i, red_control in ipairs(common.sensors[sensor_address]) do
    common.red_actions.process_controller(red_control, red_control.action)
  end
end

function common.red_actions.init_sensors()
  for sensor, red_controls in pairs(common.sensors) do
    for j, red_control in ipairs(red_controls) do
      common.red_actions.process_controller(red_control, "init")
    end
  end
end

function start(args)
  local valid_users = {["man_cubus"] = true}
  local sensitivity = 0
  for sensor_addr in pairs(common.sensors) do
    local sensor = common.cmp.proxy(sensor_addr)
    sensor.setSensitivity(sensitivity)
  end

  local function valid(name)
    return true --valid_users[name]
  end

  function common.motion_handler(...)
    local evt = {...}
    if valid(evt[6]) then
      common.red_actions.process_sensor(evt[2])
    end
  end

  common.red_actions.init_sensors()
  common.event.listen("motion", common.motion_handler)
end

function stop()
  common.event.ignore("motion", common.motion_handler)
  common.red_actions.init_sensors()
end